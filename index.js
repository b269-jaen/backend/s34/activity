const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// 1. 
app.get("/home", (request, response) => {
	response.send("Welcome to the home page");
})


// 2.
let users = [];

app.post("/signup", (request, response) => {

	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	}else{
		response.send("Please input BOTH username and password.");
	}

})


app.get("/users", (request, response) => {
	response.send(users);
})


app.delete("/delete-user", (request, response) => {
	let message;

	for (let i = 0 ; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i,1);
			message = `User ${request.body.username} has been deleted`;
			break;
		}else{
			message = "User does not exist."
		}
	}
	response.send(message);
})











app.listen(port, () => console.log(`Server running at ${port}`));